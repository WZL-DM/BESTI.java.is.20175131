import java.io.*;
public class MyCP{
    public static void main(String[] args) throws IOException {
        String file = "VirtualBox:~/BESTI.java.is.20175131/src/A.txt";
        String s =dataInputStream(file);
        FileOutputStream fps = new FileOutputStream("VirtualBox:~/BESTI.java.is.20175131/src/B.txt");
        fps.write(s.getBytes());
        fps.close();
    }
    public static String dataInputStream(String file) throws IOException {
        File file2 = new File(file);
        DataInputStream dls = new DataInputStream(new FileInputStream(file2));
        StringBuilder byData = new StringBuilder();
        byte b = 0;
        for(int i=0;i<file2.length();i++) {
            b = dls.readByte();
            String str = Integer.toBinaryString(b);
            if(str.length() == 1) {
                str = "0"+str; }
            byData.append(str.toUpperCase());
        }
        return byData.toString();
    }
}
