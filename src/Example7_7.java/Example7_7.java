public class Example7_7 {
   public static void main(String args[]) {
      CargoBoat ship = new CargoBoat();
      ship.setMinContent(131);
      int m =60;
      try{  
           ship.loading(m);
           m = 40;
           ship.loading(m);
           m = 37;
           ship.loading(m);
           m = 55;
           ship.loading(m);
      }
      catch(DangerException2 e) {
           System.out.println(e.warnMess()); 
           System.out.println("无法再装载重量是"+m+"吨的集装箱");       
      }
      finally {
          System.out.printf("货船将正点启航");
      }
  }
}
