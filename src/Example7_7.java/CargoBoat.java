public class CargoBoat {
     int realContent;  //装载的重量
     int maxContent;   //最大装载量
     int minContent;   //最小
     public void setMinContent(int c) {
         minContent = c;
     }
     public void loading(int m) throws DangerException2 {
       realContent += m;
       if(realContent<minContent) {
          throw new DangerException2(); 
       }
       System.out.println("目前装载了"+realContent+"吨货物");
     }
}

