class AdvertisementBoard { //负责创建广告牌
   public void show(Advertisement adver) {
       System.out.println(adver.getCorpName()+"的广告词如下:");
       adver.showAdvertisement(); //接口回调
  }
}
interface Advertisement { //接口
      public void showAdvertisement();
      public String getCorpName();
}
 class WZLCorp implements Advertisement { 
   public void showAdvertisement(){
      System.out.println("**************");
      System.out.printf("20175131\n20175131\n");
      System.out.println("**************");
   }
   public String getCorpName() {
      return "LONG集团" ; 
   }
}
public class Example6_6 {
   public static void main(String args[]) {
      AdvertisementBoard board = new AdvertisementBoard();
      board.show(new WZLCorp());
   
   }
}

