import java.util.*;

public class MyDC {
    private final char ADD = '+';
    private final char SUBTRACT = '-';
    private final char MUTIPLY = '*';
    private final char DIVIDE = '/';
    private Stack<Integer> stack;

    public MyDC(){
        stack = new Stack<Integer>();
    }

    public int evaluate(String expr){
        int a,b,result = 0;
        String token;
        StringTokenizer tokenizer = new StringTokenizer(expr);
        while (tokenizer.hasMoreTokens()){
            token = tokenizer.nextToken();
            if (isOperator(token)){
                b = (stack.pop().intValue());
                a = (stack.pop().intValue());
                result = evalSingleOp(token.charAt(0), a, b);
                stack.push(result);
            } else{
                stack.push((Integer.parseInt(token)));
            }
        }
        return result;
    }

    private boolean isOperator(String token){
        return (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/"));
    }

    private int evalSingleOp(char operation, int a, int b){
        int result = 0;
        switch (operation){
            case ADD:
                result = a + b;
                break;
            case SUBTRACT:
                result = a - b;
                break;
            case MUTIPLY:
                result = a * b;
                break;
            case DIVIDE:
                result = a / b;
        }
        return result;
    }
}
