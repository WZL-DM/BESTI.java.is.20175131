import java.io.*;
import java.net.*;
import java.util.Scanner;
/*
20175131王泽龙
 */
public class ClientEE {
    public static void main(String args[]) throws Exception {
        String key1="";
        int n1=-1;
        byte [] a1=new byte[128];
        try{  File f=new File("key1.dat");
            InputStream in = new FileInputStream(f);
            while((n1=in.read(a1,0,100))!=-1) {
                key1=key1+new String (a1,0,n1);
            }
            in.close();
        }
        catch(IOException e) {
            System.out.println("File read Error"+e);
        }
        String sharekey="";
        int n=-1;
        byte [] a=new byte[128];
        try{  File f=new File("Sharekey.dat");
            InputStream in = new FileInputStream(f);
            while((n=in.read(a,0,100))!=-1) {
                sharekey=sharekey+new String (a,0,n);
            }
            in.close();
        }
        catch(IOException e) {
            System.out.println("File read Error"+e);
        }
        Socket mysocket;
        DataInputStream in=null;
        DataOutputStream out=null;
        System.out.println("请输入算式：");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();//输入算式
        MyBC b=new MyBC();
        str=b.result(str);
        String secret=Encoder.AESEncode(key1, str);//客户端对表达式进行加密
        String md=DigestPass.MD5(str);//客户端提供的MD5
        key1 = Encoder.AESEncode(sharekey,key1);//客户端对密钥进行DH加密
        try{  mysocket=new Socket("127.1.0.0",2010);
            in=new DataInputStream(mysocket.getInputStream());
            out=new DataOutputStream(mysocket.getOutputStream());
            out.writeUTF(key1);
            out.writeUTF(secret);
            out.writeUTF(md);
            String  s=in.readUTF();   //in读取信息，堵塞状态
            System.out.println("客户收到服务器的回答:"+s);
            Thread.sleep(50000);
        }
        catch(Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}
