import java.util.*;
public class MyBC {
    public String result(String s) {
        Stack<String> sta = new Stack<String>();   //新建栈
        String str = "";
        StringTokenizer t=new StringTokenizer(s);
        while (t.hasMoreTokens()){    //依次遍历元素，转为后缀表达式
            String temp;
            String c;
            c=t.nextToken();
            if (c.equals("+") || c.equals("-")) {   //遇到优先级最低的“+”、“-”，弹出“（”之前的所有元素
                while (sta.size() != 0) {
                    temp = sta.pop();
                    if (temp.equals("(")) {
                        sta.push("(");
                        break;
                    }
                    str = str + temp + " ";
                }
                sta.push(c);
            } else if (c.equals("*")|| c.equals("÷")) {   //遇到优先级高的“*”、“/”，弹出“（”之前的“*”、“/”
                while (sta.size() != 0) {
                    temp = sta.pop();
                    if (temp.equals("(") || temp.equals("+") || temp.equals("-")) {
                        sta.push(temp);
                        break;
                    } else {
                        str = str + temp + " ";
                    }
                }
                sta.push(c);
            } else if (c.equals("(")) {     //遇到“(”直接入栈
                sta.push(c);
            } else if (c.equals(")")) {     //遇到“）”，弹出“（”之前的所有元素
                while (sta.size() != 0) {
                    temp = sta.pop();
                    if (temp.equals("(")) {
                        break;
                    } else {
                        str = str + temp + " ";
                    }
                }
            } else                       //遇到数字，直接存入数组
            {
                str = str + c + " ";
            }
        }
        while (sta.size()!=0){     //弹出栈中剩余的元素
            str=str+sta.pop()+" ";
        }
        return str;
    }
}
