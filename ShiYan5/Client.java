import java.io.*;
import java.net.*;
import java.lang.*;
import java.util.Scanner;
/*
  20175131 王泽龙
 */
public class Client {
    public static void main(String args[]) {
        Socket mysocket;
        DataInputStream in=null;
        DataOutputStream out=null;

        try{  mysocket=new Socket("127.1.0.0",2010);
            in=new DataInputStream(mysocket.getInputStream());
            out=new DataOutputStream(mysocket.getOutputStream());

            System.out.println("请输入算式：");
            Scanner scanner=new Scanner(System.in);
            String str=scanner.nextLine();

            MyBC b=new MyBC();
            str=b.result(str);
            out.writeUTF(str);

            String  s=in.readUTF();   //in读取信息，堵塞状态
            System.out.println("客户收到服务器的回答:"+s);
            Thread.sleep(500);
        }
        catch(Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}
