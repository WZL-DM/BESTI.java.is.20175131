import java.io.*;
import java.net.*;
/*
20175131 王泽龙
 */
public class Server {
    public static void main(String args[]) {
        int answer;
        ServerSocket serverForClient=null;
        Socket socketOnServer=null;
        DataOutputStream out=null;
        DataInputStream  in=null;

        try { serverForClient = new ServerSocket(2010);
        }
        catch(IOException e1) {
            System.out.println(e1);
        }
        try{ System.out.println("等待客户呼叫");
            socketOnServer = serverForClient.accept();
            out=new DataOutputStream(socketOnServer.getOutputStream());
            in=new DataInputStream(socketOnServer.getInputStream());
            String s=in.readUTF();
            System.out.println("服务器收到客户的提问:"+s);
            MyDC d=new MyDC();
            answer=d.evaluate(s);
            out.writeUTF(answer+"");
            Thread.sleep(500);
        }
        catch(Exception e) {
            System.out.println("客户已断开"+e);
        }
    }
}
