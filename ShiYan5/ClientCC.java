import java.io.*;
import java.net.*;
import java.lang.*;
import java.util.Scanner;

public class ClientCC {
    public static void main(String args[]) throws Exception {
        String key = "";
        int n = -1;
        byte[] a = new byte[128];
        try {
            File f = new File("key1.dat");
            InputStream in = new FileInputStream(f);
            while ((n = in.read(a, 0, 100)) != -1) {
                key = key + new String(a, 0, n);
            }
            in.close();
        } catch (IOException e) {
            System.out.println("File read Error" + e);
        }
        Socket mysocket;
        DataInputStream in = null;
        DataOutputStream out = null;
        System.out.println("请输入算式：");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();//输入算式
        MyBC b = new MyBC();
        str = b.result(str);
        String secret=Encoder.AESEncode(key,str);//客户端进行加密

        try {
            mysocket = new Socket("127.1.0.0", 2010);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            out.writeUTF(key);
            out.writeUTF(secret);
            String s = in.readUTF();   //in读取信息，堵塞状态
            System.out.println("客户收到服务器的回答:" + s);
            Thread.sleep(500);
        } catch (Exception e) {
            System.out.println("服务器已断开" + e);
        }
    }
}
